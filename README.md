# README #

## To build ##
* Install rust
* Install cargo
* Install glfw, the relevant command on mac osx is "brew tap homebrew/versions" followed by "brew install --static glfw3", unsure what the appropriate one is on other oses
* cargo run

## IntelliJ Setup ##
* Download Rust plugin (only provides basic syntax highlighting right now)
* Import Project / Use existing sources / agree a bunch

## Running in IntelliJ ##
Two hacky approaches:

1) Bash file

* Download Bash plugin
* Create a Bash run configuration
    * Give it the location of the run.sh script in the root dir
    * Give it the root dir as the working directory

2) Dummy java program

* Create run profile to run empty dummy.IDE class
* Add before launch step / run external tool / create new : program = "/usr/local/bin/cargo run" or whatever equivalent is, parameters = run, working directory = /path/to/root/dir